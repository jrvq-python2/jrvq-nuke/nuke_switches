# Nuke switch and breaker

These executable files generate "switch" and "breaker" diagrams inside Nuke's interface, to be toggled by the user by moving a StickyNote node next to them.

- The switch diagram works the same way as Nuke's switch node, toggling between two streams.
- The breaker diagram cuts or re-connects the stream


They have been tested successfully in **Nuke 11.1v1.**
***

For more info: www.jaimervq.com