# -*- coding: UTF-8 -*-
'''
Author: Jaime Rivera
File: nuke_two_switch_exec.py
Date: 2019.05.05
Revision: 2020.01.13
Copyright: Copyright 2019 Jaime Rivera

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief: Executable file to create a complex 'stream switch' in Nuke

'''

__author__ = 'Jaime Rivera <www.jaimervq.com>'
__copyright__ = 'Copyright 2019, Jaime Rivera'
__credits__ = []
__license__ = 'MIT License'
__maintainer__ = 'Jaime Rivera'
__email__ = 'jaime.rvq@gmail.com'
__status__ = 'Testing'

import nuke

# -------------------------------- TWO_SWITCH COUNT -------------------------------- #
two_switch_max = 0

for node in nuke.allNodes():
    if 'two_switch_main_' in node.name():
        two_switch_max += 1


# -------------------------------- MAIN -------------------------------- #
x_main = 500* two_switch_max
y_main = 0

n_main = nuke.nodes.Dot(name='two_switch_main_{}'.format(two_switch_max),
                        xpos=x_main,
                        ypos=y_main,
                        label='  Move group\n  from here')

main_text = "n= nuke.thisNode()" \
            "\nx=n['xpos'].value()" \
            "\ny=n['ypos'].value()" \
            "\ncenter= nuke.toNode('two_switch_center_{COUNT}')" \
            "\ncenter['xpos'].setValue(x)" \
            "\ncenter['ypos'].setValue(y-150)" \
            "\nleft= nuke.toNode('two_switch_left_{COUNT}')" \
            "\nleft['xpos'].setValue(x-100)" \
            "\nleft['ypos'].setValue(y-250)" \
            "\nleft_support= nuke.toNode('two_switch_left_support_{COUNT}')" \
            "\nleft_support['xpos'].setValue(x-100)" \
            "\nleft_support['ypos'].setValue(y-400)" \
            "\nright= nuke.toNode('two_switch_right_{COUNT}')" \
            "\nright['xpos'].setValue(x+100)" \
            "\nright['ypos'].setValue(y-250)" \
            "\nright_support= nuke.toNode('two_switch_right_support_{COUNT}')" \
            "\nright_support['xpos'].setValue(x+100)" \
            "\nright_support['ypos'].setValue(y-400)" \
            "\nslider= nuke.toNode('two_switch_slider_{COUNT}')" \
            "\nx_diff = slider['xpos'].value() - n['xpos'].value()" \
            "\nif x_diff > 150:" \
            "\n    slider['xpos'].setValue(x+250)" \
            "\nelif x_diff < 150:" \
            "\n    slider['xpos'].setValue(x+50)" \
            "\nslider['ypos'].setValue(y-150)" \
            "\nleft.setInput(0, left_support)" \
            "\nright.setInput(0, right_support)" \
            "\nn_main.setInput(0, n_center)".format(COUNT=two_switch_max)

n_main['knobChanged'].setValue(main_text)

n_center = nuke.nodes.Dot(name='two_switch_center_{}'.format(two_switch_max),
                          xpos=x_main,
                          ypos=y_main-150)

center_text = "n= nuke.thisNode()" \
              "\nn['xpos'].setValue({X})" \
              "\nn['ypos'].setValue({Y})" \
              "\nmain= nuke.toNode('two_switch_main_{COUNT}')" \
              "\nslider= nuke.toNode('two_switch_slider_{COUNT}')" \
              "\nif slider['xpos'].value()<{X_MIDDLE}:" \
              "\n    n.setInput(0, left)" \
              "\nelif slider['xpos'].value()>{X_MIDDLE}:" \
              "\n    n.setInput(0, right)".format(COUNT=two_switch_max,
                                                  X="main['xpos'].value()",
                                                  Y="main['ypos'].value() - 150",
                                                  X_MIDDLE="main['xpos'].value()+150")

n_center['knobChanged'].setValue(center_text)

n_main.setInput(0, n_center)


# -------------------------------- LEFT -------------------------------- #
n_left = nuke.nodes.Dot(name='two_switch_left_{}'.format(two_switch_max),
                        xpos=x_main - 100,
                        ypos=y_main - 250)

left_text = "n= nuke.thisNode()" \
            "\nmain= nuke.toNode('two_switch_main_{COUNT}')" \
            "\nn['xpos'].setValue({X})" \
            "\nn['ypos'].setValue({Y})".format(COUNT=two_switch_max,
                                               X="main['xpos'].value() - 100",
                                               Y="main['ypos'].value() - 250")

n_left['knobChanged'].setValue(left_text)

n_left.setInput(0, n_left)

n_left_support = nuke.nodes.Dot(name='two_switch_left_support_{}'.format(two_switch_max),
                                xpos=x_main - 100,
                                ypos=y_main - 400)

left_support_text = "n= nuke.thisNode()" \
                    "\nmain= nuke.toNode('two_switch_main_{COUNT}')" \
                    "\nn['xpos'].setValue({X})" \
                    "\nn['ypos'].setValue({Y})".format(COUNT=two_switch_max,
                                                       X="main['xpos'].value() - 100",
                                                       Y="main['ypos'].value() - 400")

n_left_support['knobChanged'].setValue(left_support_text)

n_left.setInput(0, n_left_support)


# -------------------------------- RIGHT -------------------------------- #
n_right = nuke.nodes.Dot(name='two_switch_right_{}'.format(two_switch_max),
                         xpos=x_main + 100,
                         ypos=y_main - 250)

right_text = "n= nuke.thisNode()" \
             "\nmain= nuke.toNode('two_switch_main_{COUNT}')" \
             "\nn['xpos'].setValue({X})" \
             "\nn['ypos'].setValue({Y})".format(COUNT=two_switch_max,
                                                X="main['xpos'].value() + 100",
                                                Y="main['ypos'].value() - 250")

n_right['knobChanged'].setValue(right_text)

n_right.setInput(0, n_right)

n_right_support = nuke.nodes.Dot(name='two_switch_right_support_{}'.format(two_switch_max),
                                 xpos=x_main + 100,
                                 ypos=y_main - 400)

right_support_text = "n= nuke.thisNode()" \
                     "\nmain= nuke.toNode('two_switch_main_{COUNT}')" \
                     "\nn['xpos'].setValue({X})" \
                     "\nn['ypos'].setValue({Y})".format(COUNT=two_switch_max,
                                                        X="main['xpos'].value() + 100",
                                                        Y="main['ypos'].value() - 400")

n_right_support['knobChanged'].setValue(right_support_text)

n_right.setInput(0, n_right_support)


# -------------------------------- SLIDER (STICKY NOTE) -------------------------------- #

n_slider = nuke.nodes.StickyNote(name='two_switch_slider_{}'.format(two_switch_max),
                                 xpos=x_main + 70,
                                 ypos=y_main - 150,
                                 label='<---->',
                                 note_font='arial bold',
                                 tile_color=3722305023)

slider_text = "n= nuke.thisNode()" \
              "\nmain= nuke.toNode('two_switch_main_{COUNT}')" \
              "\ncenter= nuke.toNode('two_switch_center_{COUNT}')" \
              "\nleft= nuke.toNode('two_switch_left_{COUNT}')" \
              "\nleft_support= nuke.toNode('two_switch_left_support_{COUNT}')" \
              "\nright= nuke.toNode('two_switch_right_{COUNT}')" \
              "\nright_support= nuke.toNode('two_switch_right_support_{COUNT}')" \
              "\nn['ypos'].setValue({Y_SLIDER})" \
              "\nif n['xpos'].value()<{X_MIDDLE}:" \
              "\n    center.setInput(0, left)" \
              "\n    left['tile_color'].setValue(0)" \
              "\n    left.setInput(0, left_support)" \
              "\n    left_support['tile_color'].setValue(0)" \
              "\n    right['tile_color'].setValue(3423604991)" \
              "\n    right.setInput(0, right_support)" \
              "\n    right_support['tile_color'].setValue(3423604991)" \
              "\n    n_main.setInput(0, n_center)" \
              "\nelif n['xpos'].value()>{X_MIDDLE}:" \
              "\n    center.setInput(0, right)" \
              "\n    left['tile_color'].setValue(3423604991)" \
              "\n    left.setInput(0, left_support)" \
              "\n    left_support['tile_color'].setValue(3423604991)" \
              "\n    right['tile_color'].setValue(0)" \
              "\n    right.setInput(0, right_support)" \
              "\n    right_support['tile_color'].setValue(0)" \
              "\n    n_main.setInput(0, n_center)" \
              "\nif n['xpos'].value()<{X_EXTREME_LEFT}:" \
              "\n    n['xpos'].setValue({X_EXTREME_LEFT})" \
              "\nelif n['xpos'].value()>{X_EXTREME_RIGHT}:" \
              "\n    n['xpos'].setValue({X_EXTREME_RIGHT})".format(COUNT=two_switch_max,
                                                                   Y_SLIDER="main['ypos'].value()-150",
                                                                   X_MIDDLE="main['xpos'].value()+150",
                                                                   X_EXTREME_LEFT="main['xpos'].value()+50",
                                                                   X_EXTREME_RIGHT="main['xpos'].value()+250")

n_slider['knobChanged'].setValue(slider_text)
