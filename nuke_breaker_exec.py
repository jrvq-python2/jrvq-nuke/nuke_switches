# -*- coding: UTF-8 -*-
'''
Author: Jaime Rivera
File: nuke_breaker_exec.py
Date: 2019.05.05
Revision: 2020.01.13
Copyright: Copyright 2019 Jaime Rivera

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief: Executable file to create a complex 'stream breaker' in Nuke

'''

__author__ = 'Jaime Rivera <www.jaimervq.com>'
__copyright__ = 'Copyright 2019, Jaime Rivera'
__credits__ = []
__license__ = 'MIT License'
__maintainer__ = 'Jaime Rivera'
__email__ = 'jaime.rvq@gmail.com'
__status__ = 'Testing'

import nuke

# -------------------------------- BREAKERS COUNT -------------------------------- #
breaker_max = 0

for node in nuke.allNodes():
    if 'breaker_main_' in node.name():
        breaker_max += 1


# -------------------------------- MAIN -------------------------------- #
x_main = 500* breaker_max
y_main = 0

n_main = nuke.nodes.Dot(name='breaker_main_{}'.format(breaker_max),
                        xpos=x_main,
                        ypos=y_main,
                        label='  Move group\n  from here')

main_text = "n= nuke.thisNode()" \
            "\nx=n['xpos'].value()" \
            "\ny=n['ypos'].value()" \
            "\ncenter= nuke.toNode('breaker_center_{COUNT}')" \
            "\ncenter['xpos'].setValue(x)" \
            "\ncenter['ypos'].setValue(y-150)" \
            "\nup= nuke.toNode('breaker_up_{COUNT}')" \
            "\nup['xpos'].setValue(x)" \
            "\nup['ypos'].setValue(y-250)" \
            "\nup_support= nuke.toNode('breaker_up_support_{COUNT}')" \
            "\nup_support['xpos'].setValue(x)" \
            "\nup_support['ypos'].setValue(y-400)" \
            "\nslider= nuke.toNode('breaker_slider_{COUNT}')" \
            "\nx_diff = slider['xpos'].value() - n['xpos'].value()" \
            "\nif x_diff > 100:" \
            "\n    slider['xpos'].setValue(x+150)" \
            "\nelif x_diff < 100:" \
            "\n    slider['xpos'].setValue(x+50)" \
            "\nslider['ypos'].setValue(y-150)" \
            "\nup.setInput(0, up_support)" \
            "\nn_main.setInput(0, n_center)".format(COUNT=breaker_max)

n_main['knobChanged'].setValue(main_text)

n_center = nuke.nodes.Dot(name='breaker_center_{}'.format(breaker_max),
                          xpos=x_main,
                          ypos=y_main-150)

center_text = "n= nuke.thisNode()" \
              "\nn['xpos'].setValue({X})" \
              "\nn['ypos'].setValue({Y})" \
              "\nmain= nuke.toNode('breaker_main_{COUNT}')" \
              "\nup= nuke.toNode('breaker_up_{COUNT}')" \
              "\nslider= nuke.toNode('breaker_slider_{COUNT}')" \
              "\nif slider['xpos'].value()<{X_MIDDLE}:" \
              "\n    n.setInput(0, up)" \
              "\nelif slider['xpos'].value()>{X_MIDDLE}:" \
              "\n    n.setInput(0, None)".format(COUNT=breaker_max,
                                                 X="main['xpos'].value()",
                                                 Y="main['ypos'].value() - 150",
                                                 X_MIDDLE="main['xpos'].value()+100")

n_center['knobChanged'].setValue(center_text)

n_main.setInput(0, n_center)


# -------------------------------- UP -------------------------------- #
n_up = nuke.nodes.Dot(name='breaker_up_{}'.format(breaker_max),
                        xpos=x_main,
                        ypos=y_main - 250)

up_text = "n= nuke.thisNode()" \
          "\nmain= nuke.toNode('breaker_main_{COUNT}')" \
          "\nn['xpos'].setValue({X})" \
          "\nn['ypos'].setValue({Y})".format(COUNT=breaker_max,
                                             X="main['xpos'].value()",
                                             Y="main['ypos'].value() - 250")

n_up['knobChanged'].setValue(up_text)

n_up.setInput(0, n_up)

n_up_support = nuke.nodes.Dot(name='breaker_up_support_{}'.format(breaker_max),
                                xpos=x_main,
                                ypos=y_main - 400)

up_support_text = "n= nuke.thisNode()" \
                  "\nmain= nuke.toNode('breaker_main_{COUNT}')" \
                  "\nn['xpos'].setValue({X})" \
                  "\nn['ypos'].setValue({Y})".format(COUNT=breaker_max,
                                                     X="main['xpos'].value()",
                                                     Y="main['ypos'].value() - 400")

n_up_support['knobChanged'].setValue(up_support_text)

n_up.setInput(0, n_up_support)


# -------------------------------- SLIDER (STICKY NOTE) -------------------------------- #

n_slider = nuke.nodes.StickyNote(name='breaker_slider_{}'.format(breaker_max),
                                 xpos=x_main + 50,
                                 ypos=y_main - 150,
                                 label='CLOSED',
                                 note_font='arial bold',
                                 tile_color=484774655)

slider_text = "n= nuke.thisNode()" \
              "\nmain= nuke.toNode('breaker_main_{COUNT}')" \
              "\ncenter= nuke.toNode('breaker_center_{COUNT}')" \
              "\nup= nuke.toNode('breaker_up_{COUNT}')" \
              "\nup_support= nuke.toNode('breaker_up_support_{COUNT}')" \
              "\nn['ypos'].setValue({Y_SLIDER})" \
              "\nif n['xpos'].value()<{X_MIDDLE}:" \
              "\n    center.setInput(0, up)" \
              "\n    center['hide_input'].setValue(False)" \
              "\n    up['tile_color'].setValue(0)" \
              "\n    up.setInput(0, up_support)" \
              "\n    center['tile_color'].setValue(0)" \
              "\n    n_main.setInput(0, n_center)" \
              "\n    n['tile_color'].setValue(484774655)" \
              "\n    n['label'].setValue('CLOSED')" \
              "\nelif n['xpos'].value()>{X_MIDDLE}:" \
              "\n    center.setInput(0, None)" \
              "\n    center['hide_input'].setValue(True)" \
              "\n    up['tile_color'].setValue(3423604991)" \
              "\n    up.setInput(0, up_support)" \
              "\n    center['tile_color'].setValue(3423604991)" \
              "\n    n_main.setInput(0, n_center)" \
              "\n    n['tile_color'].setValue(3423604991)" \
              "\n    n['label'].setValue('BROKEN')" \
              "\nif n['xpos'].value()<{X_EXTREME_LEFT}:" \
              "\n    n['xpos'].setValue({X_EXTREME_LEFT})" \
              "\nelif n['xpos'].value()>{X_EXTREME_RIGHT}:" \
              "\n    n['xpos'].setValue({X_EXTREME_RIGHT})".format(COUNT=breaker_max,
                                                                   Y_SLIDER="main['ypos'].value()-150",
                                                                   X_MIDDLE="main['xpos'].value()+100",
                                                                   X_EXTREME_LEFT="main['xpos'].value()+50",
                                                                   X_EXTREME_RIGHT="main['xpos'].value()+150")

n_slider['knobChanged'].setValue(slider_text)
